
package homework1;

/**
 *
 * @author Umberto
 */
public class Main {

    private static int N_TRAINSET = 9;
    private static int N_INPUTS = 64;
    private static int N_TESTSET = 3;
    private static int NUMBER_OF_EPOCHS = 2000;

    private static double[][] dataset
            = {{0, 0, 0, 1, 1, 0, 0, 0,
                0, 0, 1, 0, 0, 1, 0, 0, // ‘A’
                0, 1, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1
            },
               {1, 1, 1, 1, 1, 1, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,// ‘B’
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 1, 1, 1, 1, 1, 0, 0,
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 1, 1, 1, 1, 1, 0, 0},
               {0, 0, 1, 1, 1, 1, 1, 1, // ‘C’
                0, 1, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0, 0, 0,
                0, 0, 1, 1, 1, 1, 1, 1},
               {1, 1, 1, 1, 1, 1, 0, 0, // ‘D’
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 1, 0,
                1, 1, 1, 1, 1, 1, 0, 0},
               {1, 1, 1, 1, 1, 1, 1, 1, // ‘E’
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1},
               {1, 1, 1, 1, 1, 1, 1, 1,// ‘F’
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0},
               {0, 0, 1, 1, 1, 1, 1, 1,// ‘G’
                0, 1, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                0, 1, 0, 0, 0, 0, 0, 1,
                0, 0, 1, 1, 1, 1, 1, 1},
               {1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1, // ‘H’
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 1},
               {0, 0, 1, 1, 1, 1, 1, 0, // I
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 1, 1, 1, 1, 1, 0}};

    private static double[][] testSet
            = {{1, 0, 0, 0, 0, 0, 0, 1,
                0, 1, 0, 0, 0, 0, 1, 0, // ‘X’
                0, 0, 1, 0, 0, 1, 0, 0,
                0, 0, 0, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 0, 0, 0,
                0, 0, 1, 0, 0, 1, 0, 0,
                0, 1, 0, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 1,},
               {0, 1, 0, 0, 0, 0, 0, 1,
                0, 0, 1, 0, 0, 0, 1, 0, // ‘Y’
                0, 0, 0, 1, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,},
               {1, 1, 1, 1, 1, 1, 1, 1,// ‘Z’
                0, 0, 0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1}};
    private static double[][] targetTest = {{0, 0}, {0, 0}, {0, 0}};
    //target values
    private static double[][] datatrue
            = {{0, 1}, {1, 0}, {1, 1},
               {0, 0}, {0, 0}, {0, 0},
               {0, 0}, {0, 0}, {0, 0}};

    public static void main(String[] args) {
        BackPropNN nn = new BackPropNN();

        //add first layer with two neurons and 36 inputs
        nn.addLayer(3, N_INPUTS);

        //second layer
        nn.addLayer(3, 3);

        //third layer
        nn.addLayer(2, 3);

        //nn.printLayers();
        //nn.printLayerConnections();
        //System.out.printf("%d - %d\n",datatrue.length,datatrue[0].length);
        nn.train(dataset, N_TRAINSET, datatrue, NUMBER_OF_EPOCHS);

        //nn.test(testSet, N_TESTSET, targetTest);
        nn.robustnessTest(dataset, N_TRAINSET, datatrue);
        //gatherStats(nn);
    }

    private static void gatherStats(BackPropNN nn) {
        int NUMB_OF_RUNS = 2000;
        long sum;
        for (int e = 1; e * NUMBER_OF_EPOCHS < 2001; e *= 2) {
            sum = 0;
            for (int i = 0; i < NUMB_OF_RUNS; i++) {
                long startTime = System.currentTimeMillis();
                nn.train(dataset, N_TRAINSET, datatrue, NUMBER_OF_EPOCHS * e);
                long endTime = System.currentTimeMillis();
                long totalTime = endTime - startTime;
                sum += totalTime;
            }
            System.out.
                    printf("Execution time for %d epoch is: %d milliseconds\n", e * NUMBER_OF_EPOCHS, sum / NUMB_OF_RUNS);
        }
    }
}
